import numpy as np
import matplotlib.pyplot as plt
import os


#############################################################
#Convert American to Decimal Odds
def convertAmericanDec(odds):
    if odds>0:
        dec = (odds/100)+1
    else:
        dec = (100/abs(odds))+1
    return dec
#############################################################
#Convert Decimal to American Odds
def convertDecAmerican(odds):
    if odds>=2.0:
        american = (odds-1)*100
    else:
        american =(-100)/(odds-1)
    return american

#############################################################
# Calculate the Vig
def calcVig(fav,dog):
    impdog = 100/(100+dog)
    impfav = -fav/(100-fav)
    print(impfav)
    print(impdog)

    return ((impdog+impfav-1)*100)

#############################################################
# Implied Probabity
def impliedProb(fav,dog):
    impdog = 100/(100+dog)
    impfav = -fav/(100-fav)
    total = impdog+impfav
    imp = []
    imp.append(impfav/total)
    imp.append(impdog / total)

    return imp


# #############################################################
# # Calculate Expected Growth
# def ExpectedGrowth(fav,dog):
#     null

#################################################################
#Kelly Criterion
def Kelly_Crit(b, p):
    f = ((p*b)-(1-p))/b
    return f

#############################################################
#returns the amount you should bet based on your bankroll, odds and predictions
def getBetAmount(bank_roll,odds,prediction):

    amount = Kelly_Crit((odds-1), prediction)*bank_roll
    if amount>0:
        return amount
    else:
        return 0


####################################################
#returns the amount of money won as the result of betting per game in an array
def bankrollOutcome(bankroll,home_odds,away_odds,pred,winner_home):

    # Calculation to Decide Bet Amount
    home = getBetAmount(bankroll,home_odds,pred)
    away=  getBetAmount(bankroll,away_odds,(1-pred))
    max_bet = max(home,away)

    #if we bet something
    if max_bet > .5*bankroll:
        max_bet = .5*bankroll
    if max_bet > 0:
        #if we bet on the home team
        if max_bet == home:
            team = 1
            odds = home_odds
        else:
            team = 0
            odds = away_odds
        print("max bet " + str(max_bet)+" odds " +str(odds))
        if team == winner_home:
            winnings = max_bet*odds
            bankroll= bankroll+ winnings
            print("Won amount " +str(winnings) +" new bankroll " + str(bankroll))
        else:
            bankroll=bankroll-max_bet
            print("Lost amount " + str(max_bet) + " new bankroll " + str(bankroll))


    return bankroll


#######################################################################
# Based on your money list
def plotBetting(money):
    games = [i for i in range(len(money))]
    for k in range(0,len(games),100):
        plt.text(games[k], money[k], '(' + str(round(money[k])) + ')')
    plt.title("Bank Roll vs Playoff Games")
    plt.xlabel("Game")
    plt.ylabel("BankRoll ")
    plt.plot(games, money, 'r-o')
    plt.show()

###########################################################################

