import sqlite3
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import math
import Bets
import datetime
import os.path
np.random.seed(7)


game_id_index = 0
game_date_index = 1
away_team_index = 2
home_team_index = 3
team_index = 4

home_game_index = 0
away_game_index = 0

win_index = 127
loss_index = 128
at_bat= 5
home_or_away_index = 134



def normalize_stats(team,season):
    normalized_stats = []
    games_played = 0
    team_stats = [0 for x in range((home_or_away_index-at_bat))]

    for j in range(0, len(season)):
        if team == season[j][team_index]:
            game_stats = season[j][team_index+1:home_or_away_index]
            games_played = games_played + 1
            for z in range(0,len(team_stats)):
                team_stats[z] = (team_stats[z] + game_stats[z])

    if games_played ==0:
        return team_stats
    else:
        normalized_stats = [x / games_played for x in team_stats]
        return normalized_stats


def get_Prediction_Stats(team,home_or_away):
    team_sql = "'"+team+"'"

    begin_date = "'" + str(int(datetime.date.today().year)) + "-01-01'"
    end_date = "'" + str(datetime.date.today()) + "'"

    sql = '''SELECT * from MLB_Team_Stats_Regular where Game_Date >'''+begin_date+''' and Game_Date <'''+end_date+''' and Team == '''+team_sql+''' order by Game_Date'''

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    cur.execute(sql)
    season = cur.fetchall()
    connection.close()
    normalized=[]
    if season:
        normalized = normalize_stats(team, season)+  list(season[-1][home_or_away_index+1:]) # +[home_or_away]+ list(season[-1][home_or_away_index+1:])
    else:
        print("No stats for that team")
        exit(-1)
    return normalized

def predict_MLB_Game(home_team,away_team):
    model = load_model('../Machine_Learning/MLB_Model.h5')
    home_stats = get_Prediction_Stats(home_team,1)
    away_stats = get_Prediction_Stats(away_team,0)
    data =  np.array([np.array(home_stats+away_stats)])
    prediction = model.predict(data)

    return prediction


def get_MLB_Train_Data(low_year,high_year):
    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    train_stats = []
    season= []
    for year in range(low_year,high_year+1):
        season.clear()
        begin_year = "'"+str(year) + "-01-01'"
        end_year = "'"+str(year) + "-12-30'"
        sql = '''SELECT * from MLB_Team_Stats_Regular where GAME_DATE >''' + str(begin_year) + ''' and Game_Date <''' + str(end_year) + ''' order by GAME_Date '''
        cur.execute(sql)
        season = cur.fetchall()

        print("Got MLB regular season" + str(begin_year) + " to " + str(end_year))

        for i in range(0,len(season)):
            print("Game "+str(i)+ " out of " + str(len(season)))
            home_team = season[i][home_team_index]
            away_team = season[i][away_team_index]
            game_id= season[i][game_id_index]

            for j in range(0,len(season)):
                #find corresponding game
                if game_id== season[j][game_id_index] and i!= j:
                    #find home team and away team index
                    if season[i][home_team_index] == season[i][team_index]:
                        home_game_index = i
                        away_game_index = j
                    else:
                        home_game_index = j
                        away_game_index = i

                    home_stats = normalize_stats(team=home_team, season=season[0:home_game_index])+list(season[home_game_index][home_or_away_index+1:])
                    away_stats = normalize_stats(team=away_team, season=season[0:away_game_index])+list(season[away_game_index][home_or_away_index+1:])


                    home_stats.insert(0, season[home_game_index][win_index])
                    home_stats.insert(1, season[away_game_index][win_index])
                    train_stats.append(home_stats+away_stats)


   #  sql='''insert into NHL_Train_Stats(
   #  Field1,
   #  Field2,
   #  Field3,
   #  Field4,
   #  Field5,
   #  Field6,
   #  Field7,
   #  Field8,
   #  Field9,
   #  Field10,
   #  Field11,
   #  Field12,
   #  Field13,
   #  Field14,
   #  Field15,
   #  Field16,
   #  Field17,
   #  Field18,
   #  Field19,
   #  Field20,
   #  Field21,
   #  Field22,
   #  Field23,
   #  Field24,
   #  Field25,
   #  Field26,
   #  Field27,
   #  Field28,
   #  Field29,
   #  Field30,
   #  Field31,
   #  Field32,
   #  Field33,
   #  Field34,
   #  Field35,
   #  Field36,
   #  Field37,
   #  Field38,
   #  Field39,
   #  Field40,
   #  Field41,
   #  Field42,
   #  Field43,
   #  Field44,
   #  Field45,
   #  Field46,
   #  Field47,
   #  Field48,
   #  Field49,
   #  Field50,
   #  Field51,
   #  Field52,
   #  Field53,
   #  Field54,
   #  Field55,
   #  Field56,
   #  Field57,
   #  Field58,
   #  Field59
   # ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
   #  cur.executemany(sql,train_stats)
    connection.commit()
    connection.close()

    return train_stats


def train_Model():
    data = get_MLB_Train_Data(2018,2018)
    print("Training on data set size " +str(len(data)))
    dev = math.floor(.9*len(data))
    data = np.asarray(data)

    scaler = MinMaxScaler()
    scaler.fit(data)
    MinMaxScaler(copy=True, feature_range=(0, 1))
    data = scaler.transform(data)

    X_train = data[:dev,2:]
    Y_train = data[:dev,:2]
    X_dev = data[dev:,2:]
    Y_dev = data[dev:,:2]

    # hyperparameters
    epochs = 250
    batch_size = 162
    layer1Size = 528
    layer2Size = 264
    layer3Size = 10
    layer4Size = 132
    layer5Size = 66
    layer6Size = 33
    layer7Size = 16
    # dataset size
    inpDim = X_train.shape[1]

    # create model
    model = Sequential()
    model.add(Dense(layer1Size, input_dim = inpDim, activation= 'relu'))
    model.add(Dropout(0.3))
    model.add(Dense(layer2Size, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(layer3Size, activation='relu'))
    model.add(Dropout(0.1))
    # model.add(Dense(layer4Size, activation='relu'))
    # model.add(Dropout(0.3))
    # model.add(Dense(layer5Size, activation='relu'))
    # model.add(Dropout(0.2))
    # model.add(Dense(layer6Size, activation='relu'))
    # model.add(Dropout(0.1))
    # model.add(Dense(layer7Size, activation='relu'))
    model.add(Dense(2, activation='softmax'))

    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Fit the model
    history = model.fit(X_train, Y_train, epochs = epochs, batch_size = batch_size)

    # Evaluate the model
    scores = model.evaluate(X_dev, Y_dev)
    print("Training Finished ")
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    print("DEV SET ACCURACY")
    print("\n%.2f%%" % (scores[1]*100))
    model.save('MLB_Model.h5')
    # print(model.layers[0].get_weights())

def graph_winnings(bankroll):
    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    sql = '''SELECT * from NHL_Team_Stats_Regular where GAME_DATE >'2018-06-01' and Game_Date <'2019-06-01' order by GAME_Date '''
    cur.execute(sql)
    season = cur.fetchall()
    model = load_model('../Machine_Learning/NHL_Model.h5')
    train_stats=[]
    money=[]

    for i in range(0, len(season)):
        # print("Game " + str(i) + " out of " + str(len(season)))
        home_team = season[i][home_team_index]
        away_team = season[i][away_team_index]
        game_id = season[i][game_id_index]

        for j in range(0, len(season)):
            # find corresponding game
            if game_id == season[j][game_id_index] and i != j:
                # find home team and away team index
                if season[i][home_team_index] == season[i][team_index]:
                    home_game_index = i
                    away_game_index = j
                else:
                    home_game_index = j
                    away_game_index = i

                home_stats = normalize_stats(team=home_team, season=season[0:home_game_index]) + list(
                    season[home_game_index][home_or_away_index:])
                away_stats = normalize_stats(team=away_team, season=season[0:away_game_index]) + list(
                    season[away_game_index][home_or_away_index:])



                if len(home_stats) > 20 and len(away_stats) > 20:
                    data = np.array([np.array(home_stats+away_stats)])
                    prediction = model.predict(data)
                    prediction = float(prediction[0,0])
                    bankroll = Bets.bankrollOutcome(bankroll=bankroll,
                                                    home_odds=season[home_game_index][moneyline_close_index],
                                                    away_odds=season[away_game_index][moneyline_close_index],
                                                    pred=prediction, winner_home=season[home_game_index][win_index])
                    print("Bankroll "+ str(bankroll))
                    money.append(bankroll)



    Bets.plotBetting(money)

















