import sqlite3
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
import numpy as np
from numpy import savetxt
import math
from Betting import Bets
import datetime
import os.path
np.random.seed(7)


team_index = 4
home_team_index = 3
away_team_index = 2
game_id_index = 0
home_game_index = 0
away_game_index = 0
win_index = 5
loss_index = 6
hit_index = 26
home_or_away_index = 27
five_game_avg_index = 28
moneyline_open_index = 30
moneyline_close_index = 31
game_date_index = 1

def get_differential_stats(teamhomestats, teamawaystats):
    differential_stats = [0 for j in range(len(teamhomestats))]
    for i in range(len(teamhomestats)):
        differential_stats[i] = teamhomestats[i]-teamawaystats[i]

    return differential_stats


def normalize_stats(team,season):
    normalized_stat= []
    games_played = 0
    team_stats = [0 for x in range((hit_index-win_index)+1)]

    for j in range(0, len(season)):
        if team == season[j][team_index]:
            game_stats = season[j][win_index:hit_index+1]
            games_played = games_played + 1
            for z in range(0,len(team_stats)):

                team_stats[z] = (team_stats[z] + game_stats[z])

    if games_played ==0:
        return team_stats
    else:
        normalized_stat = [x / games_played for x in team_stats]
        return normalized_stat

def get_Prediction_Stats(team,home_or_away,odds_stats):
    team_sql = "'"+team+"'"

    if datetime.date.today().month > 9:
        begin_date = "'" + str(int(datetime.date.today().year)) + "-09-01'"
    else:
        begin_date = "'" + str(int(datetime.date.today().year) - 1) + "-09-01'"

    end_date = "'" + str(datetime.date.today()) + "'"

    sql = '''SELECT * from NHL_Team_Stats_Regular where Game_Date >'''+begin_date+''' and Game_Date <'''+end_date+''' and Team == '''+team_sql+''' order by Game_Date'''

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    cur.execute(sql)
    season = cur.fetchall()
    connection.close()
    normalized=[]
    if season:
        normalized = normalize_stats(team, season)+[home_or_away]+list(season[-1][five_game_avg_index:moneyline_open_index])+ odds_stats
    return normalized

def predict_Game(home_team,away_team,home_odds,away_odds):
    model = load_model('../Machine_Learning/NHL_Model.h5')
    home_stats = get_Prediction_Stats(home_team,1,home_odds)
    away_stats = get_Prediction_Stats(away_team,0,away_odds)
    data =  np.array([np.array(home_stats+away_stats)])
    prediction = model.predict(data)
    print(prediction)
    return prediction[0]

def test_prediciton(array):
    model = load_model('../Machine_Learning/NHL_Model.h5')
    data = np.array([np.array(array)])
    prediction = model.predict(data)

    return float(prediction[0, 0])

def get_NHL_Train_Data(low_year,high_year):
    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    train_stats = []
    season= []
    unique = []
    for year in range(low_year,high_year):
        season.clear()
        begin_year = "'"+str(year) + "-06-01'"
        end_year = "'"+str(year+1) + "-06-01'"
        sql = '''SELECT * from NHL_Team_Stats_Regular where GAME_DATE >''' + str(begin_year) + ''' and Game_Date <''' + str(end_year) + ''' order by GAME_Date '''
        cur.execute(sql)
        season = cur.fetchall()

        print("Got NHL regular season" + str(begin_year) + " to " + str(end_year))

        for i in range(0,len(season)):
            print("Game "+str(i)+ " out of " + str(len(season)))
            home_team = season[i][home_team_index]
            away_team = season[i][away_team_index]
            game_id= season[i][game_id_index]

            for j in range(0,len(season)):
                #find corresponding game
                if game_id== season[j][game_id_index] and i!= j:
                    #find home team and away team index
                    if season[i][home_team_index] == season[i][team_index]:
                        home_game_index = i
                        away_game_index = j
                    else:
                        home_game_index = j
                        away_game_index = i


                    home_stats = normalize_stats(team=home_team, season=season[0:home_game_index])+list(season[home_game_index][home_or_away_index:])
                    away_stats = normalize_stats(team=away_team, season=season[0:away_game_index])+list(season[away_game_index][home_or_away_index:])

                    # home_stats.insert(0, season[home_game_index][win_index]) #original
                    # home_stats.insert(1, season[away_game_index][win_index])
                    home_stats.insert(0, season[home_game_index][win_index])  # modified
                    away_stats.insert(0, season[away_game_index][win_index])


                    diff_stats = get_differential_stats(home_stats, away_stats) #modified

                    if not game_id in unique:
                        #train_stats.append(home_stats+away_stats) #original
                        train_stats.append(diff_stats)
                        unique.append(game_id)


   #  sql='''insert into NHL_Train_Stats(
   #  Field1,
   #  Field2,
   #  Field3,
   #  Field4,
   #  Field5,
   #  Field6,
   #  Field7,
   #  Field8,
   #  Field9,
   #  Field10,
   #  Field11,
   #  Field12,
   #  Field13,
   #  Field14,
   #  Field15,
   #  Field16,
   #  Field17,
   #  Field18,
   #  Field19,
   #  Field20,
   #  Field21,
   #  Field22,
   #  Field23,
   #  Field24,
   #  Field25,
   #  Field26,
   #  Field27,
   #  Field28,
   #  Field29,
   #  Field30,
   #  Field31,
   #  Field32,
   #  Field33,
   #  Field34,
   #  Field35,
   #  Field36,
   #  Field37,
   #  Field38,
   #  Field39,
   #  Field40,
   #  Field41,
   #  Field42,
   #  Field43,
   #  Field44,
   #  Field45,
   #  Field46,
   #  Field47,
   #  Field48,
   #  Field49,
   #  Field50,
   #  Field51,
   #  Field52,
   #  Field53,
   #  Field54,
   #  Field55,
   #  Field56,
   #  Field57,
   #  Field58,
   #  Field59
   # ) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
   #  cur.executemany(sql,train_stats)
    # connection.commit()
    #connection.close()

    return train_stats


def train_Model(low,high):
    data = get_NHL_Train_Data(low,high)
    savetxt('data.csv', data, delimiter=',')
    print("Training on data set size " + str(len(data)))
    data = np.asarray(data)


    data = np.delete(data,3,1) #modified comment if not doing differential
    data = np.delete(data,-7,1)

    scaler = MinMaxScaler()
    scaler.fit(data)
    MinMaxScaler(copy=True, feature_range=(0, 1))
    data = scaler.transform(data)
    savetxt('datascaled.csv', data, delimiter=',')
    # X = data[:, 2:] #original
    # Y = data[:, :2]


    X = data[:, 1:] #modified
    Y = data[:, :1]

    bestfeatures = SelectKBest(score_func=chi2, k=10)
    fit = bestfeatures.fit(X, Y)
    print(fit.scores_)
    inpDim = data.shape[1]
    print(inpDim)

    pca = PCA(n_components=6)
    principalComponents = pca.fit_transform(X)
    new_X = principalComponents
    #new_X = X
    savetxt('pca.csv', new_X, delimiter=',')



    X_train, X_test, Y_train, Y_test = train_test_split(new_X, Y, test_size=0.2, shuffle=False)


    # hyperparameters
    epochs = 20
    batch_size = 1
    layer1Size = 18
    layer2Size = 3
    # layer3Size = 9
    # layer4Size = 13
    # layer5Size = 10

    # dataset size
    inpDim = X_train.shape[1]
    print(inpDim)

    # create model
    model = Sequential()
    model.add(Dense(layer1Size, input_dim = inpDim, activation= 'relu'))
    #model.add(Dropout(0.2))
    model.add(Dense(layer2Size, activation='relu'))
    #model.add(Dropout(0.2))
    #model.add(Dense(layer3Size, activation='relu'))
    #model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Fit the model
    history = model.fit(X_train, Y_train, epochs = epochs, batch_size = batch_size)

    # Evaluate the model
    train_score = model.evaluate(X_train,Y_train)
    scores = model.evaluate(X_test, Y_test)

    print("Training Finished ")
    print("\n%s: %.2f%%" % (model.metrics_names[1], train_score[1]*100))
    print("Test SET ACCURACY")
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    model.save('NHL_Model.h5')

def graph_winnings(bankroll):
    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    sql = '''SELECT * from NHL_Team_Stats_Regular where GAME_DATE >'2018-06-01' and Game_Date <'2019-06-01' order by GAME_Date '''
    cur.execute(sql)
    season = cur.fetchall()
    model = load_model('../Machine_Learning/NHL_Model.h5')
    train_stats=[]
    money=[]

    for i in range(0, len(season)):
        # print("Game " + str(i) + " out of " + str(len(season)))
        home_team = season[i][home_team_index]
        away_team = season[i][away_team_index]
        game_id = season[i][game_id_index]

        for j in range(0, len(season)):
            # find corresponding game
            if game_id == season[j][game_id_index] and i != j:
                # find home team and away team index
                if season[i][home_team_index] == season[i][team_index]:
                    home_game_index = i
                    away_game_index = j
                else:
                    home_game_index = j
                    away_game_index = i

                home_stats = normalize_stats(team=home_team, season=season[0:home_game_index]) + list(
                    season[home_game_index][home_or_away_index:])
                away_stats = normalize_stats(team=away_team, season=season[0:away_game_index]) + list(
                    season[away_game_index][home_or_away_index:])



                if len(home_stats) > 20 and len(away_stats) > 20:
                    data = np.array([np.array(home_stats+away_stats)])
                    prediction = model.predict(data)
                    prediction = float(prediction[0,0])
                    bankroll = Bets.bankrollOutcome(bankroll=bankroll,
                                                    home_odds=season[home_game_index][moneyline_close_index],
                                                    away_odds=season[away_game_index][moneyline_close_index],
                                                    pred=prediction, winner_home=season[home_game_index][win_index])
                    print("Bankroll "+ str(bankroll))
                    money.append(bankroll)



    Bets.plotBetting(money)



train_Model(2017,2018)
















