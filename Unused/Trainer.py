from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense
from keras.layers import Dropout
import numpy as np
import os

row_dict = {"VGK": 1, "VAN": 2, "CGY": 3, "EDM": 4, "SJS": 5, "LAK": 6, "MIN": 7, "DAL": 8,
"STL": 9, "CBJ": 10, "MTL": 11, "BUF": 12, "NJD": 13, "OTT":14, "TBL": 15,"DET" :16,"FLA" :17,
"COL" :18,"NSH" :19,"CAR" :20,"TOR" : 21,"WSH" :22,"NYI" :23,"BOS" :24,"ARI" :25,"CHI" :26,
"ANA" :27,"WPG" :28,"NYR": 29,"PHI":30,"PIT" :36}

def splitArray(array):
    Y = array[:,0:1]
    X = array[:,1:]
    return X, Y

# create testing and dev set from data
def splitSet(array):
    cutoff = int(array.shape[0]*0.95)
    TestSet = array[0:cutoff, :]
    DevSet = array[cutoff:array.shape[0], :]
    X_dev, Y_dev = splitArray(DevSet)
    X_train, Y_train = splitArray(TestSet)
    return X_dev, Y_dev, X_train, Y_train

# def loadTrainingData():
#     os.chdir(os.path.dirname(__file__))
#     dataset = np.loadtxt("trainingData_V5.csv", delimiter=",")
#     X_dev, Y_dev, X_train, Y_train = splitSet(dataset)
#     np.savetxt("X_dev.csv", X_dev, delimiter=",", fmt="%.5f")
#     np.savetxt("Y_dev.csv", Y_dev, delimiter=",", fmt="%.5f")
#     np.savetxt("X_train.csv", X_train, delimiter=",", fmt="%.5f")
#     np.savetxt("Y_train.csv", Y_train, delimiter=",", fmt="%.5f")


def train_Model():
    print("Training Model")
    dataset = np.loadtxt("trainingData_V5.csv", delimiter=",")
    X_dev, Y_dev, X_train, Y_train = splitSet(dataset)

    # hyperparameters
    epochs = 250
    batch_size = 50
    layer1Size = 20
    layer2Size = 20
    layer3Size = 10

    # dataset size
    inpDim = X_train.shape[1]

    # create model
    model = Sequential()
    model.add(Dense(layer1Size, input_dim = inpDim, activation= 'relu'))
    model.add(Dropout(0.3))
    model.add(Dense(layer2Size, activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(layer2Size, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    # Fit the model
    history = model.fit(X_train, Y_train, epochs = epochs, batch_size = batch_size)

    # Evaluate the model
    scores = model.evaluate(X_dev, Y_dev)
    print("Training Finished ")
    # print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100)100)
    print("DEV SET ACCURACY")
    print("\n%.2f%%" % (scores[1]*100))
    model.save('TrainedModel.h5')


def getPrediction(hometeam, awayteam):
    os.chdir(os.path.dirname(__file__))
    array = np.loadtxt("X_dev.csv", delimiter=",")
    model = load_model('TrainedModel.h5')
    m = array.shape[0]
    n = array.shape[1]
    homestats = array[m-row_dict[hometeam], 0:n]
    awaystats = array[m-row_dict[awayteam], 0:n]
    statsDiff = homestats - awaystats
    statsDiff2 = statsDiff[np.newaxis]
    result = model.predict(statsDiff2)
    return result


