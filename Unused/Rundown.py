import requests
import csv
import Common
from Common import GameBet
from Common import Rundown_Sport_dict
import datetime
# These code snippets use an open-source library. http://unirest.io/python


def getRundownOdds(sport):

    response = requests.get("https://therundown-therundown-v1.p.mashape.com/sports/"+str(Rundown_Sport_dict[sport])+"/events?include=scores",
      headers={
        "X-Mashape-Key": "3qwlsXrxcMmshnDQNsJcwqwSivMvp1mLxfijsnbTZSXbRyt8FW",
        "Accept": "application/json"
      }
    )


    sportbets = response.json()
    print(sportbets)
    Games = []
    odds_home = []
    odds_away = []
    affiliate = []
    odds=[]
    date = datetime.datetime.now()

    for event in range(len(sportbets['events'])):
        home_team = sportbets['events'][event]['teams'][0]['name']
        away_team = sportbets['events'][event]['teams'][1]['name']
        gametime = sportbets['events'][event]['event_time']

        for site in sportbets['events'][event]['lines']:
            odds_home.append(sportbets['events'][event]['lines'][site]['moneyline']['moneyline_home'])
            odds_away.append(sportbets['events'][event]['lines'][site]['moneyline']['moneyline_away'])
            affiliate.append(sportbets['events'][event]['lines'][site]['affiliate']["affiliate_name"])

        for i in affiliate:
            odds.append(Common.Odd_Container(affiliate[i],odds_home[i],odds_away[i]))

        print(odds)
        Games.append(GameBet(date,gametime,home_team,away_team,odds))
        odds.clear()
        odds_away.clear()
        odds_home.clear()
        affiliate.clear()


    for i in range(len(Games)):
        print(sport)
        print(j.site for j in Games[i].odds)
        print(Games[i].team_home)
        print(j.home_odds for j in Games[i].odds)
        print(Games[i].team_away)
        print(j.away_odds for j in Games[i].odds)
        print()
    return Games

##############################################################
def append_csv(name, array):
    with open(name, 'a') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_file.writerow(array)
    csv_file.close()
################################################################
# getRundownOdds("NCAA Football")
# getRundownOdds("NCAA Men's Basketball")
# getRundownOdds("NBA")
# getRundownOdds("NFL")

#############################################################
def containsIndex(value, array):
    for i in range(len(array)):
        if array[i].site == value:
            return i
        else:
            return -1
def higher_odds(odd1,odd2):
    if odd1 > odd2:
        return odd1
    else:
        return odd2











x = getRundownOdds("NHL")
print(x)










