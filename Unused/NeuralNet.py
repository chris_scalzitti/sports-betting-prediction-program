import tensorflow as tf
from tensorflow.python.framework import ops
import numpy as np

# sigmoid activation function
def sigmoid(z):
    x = tf.placeholder(tf.float32, name = 'y')
    y = tf.sigmoid(x)
    with tf.Session() as sess:
        result = sess.run(y, feed_dict={x:z})
    return result

# computes the cross entropy cost function
def cost(logits, labels):
    x = tf.placeholder(tf.float32, name = 'x')
    y = tf.placeholder(tf.float32, name = 'y')
    # computes the cross entropy cost function
    costfunc = tf.nn.sigmoid_cross_entropy_with_logits(logits=x, labels=y)

    with tf.Session() as sess:
        costout = sess.run(costfunc, feed_dict={x:logits, y:labels})
    return costout

def create_placeholders(size_x,size_y):
    x = tf.placeholder(tf.float32,[size_x,None],name='x')
    y = tf.placeholder(tf.float32, [size_y, None], name='y')
    return x, y

def initialize_parameters(inpLayer, hidden1, hidden2, outputLayer):
    W1 = tf.get_variable("W1", [hidden1, inpLayer], initializer = tf.contrib.layers.xavier_initializer())
    b1 = tf.get_variable("b1", [hidden1, 1], initializer = tf.zeros_initializer())
    W2 = tf.get_variable("W2", [hidden2, hidden1], initializer = tf.contrib.layers.xavier_initializer())
    b2 = tf.get_variable("b2", [hidden2, 1], initializer = tf.zeros_initializer())
    W3 = tf.get_variable("W3", [outputLayer, hidden2], initializer = tf.contrib.layers.xavier_initializer())
    b3 = tf.get_variable("b3", [outputLayer, 1], initializer = tf.zeros_initializer())

    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2,
                  "W3": W3,
                  "b3": b3}
    return parameters

def forward_propagation(X, parameters):
    W1 = parameters['W1']
    b1 = parameters['b1']
    W2 = parameters['W2']
    b2 = parameters['b2']
    W3 = parameters['W3']
    b3 = parameters['b3']
    Z1 = tf.add(tf.matmul(W1, X), b1)  # Z1 = np.dot(W1, X) + b1
    A1 = tf.nn.relu(Z1)  # A1 = relu(Z1)
    Z2 = tf.add(tf.matmul(W2, A1), b2)  # Z2 = np.dot(W2, a1) + b2
    A2 = tf.nn.relu(Z2)  # A2 = relu(Z2)
    Z3 = tf.add(tf.matmul(W3, A2), b3)  # Z3 = np.dot(W3,Z2) + b3
    return Z3

def compute_cost(Z3, Y):
    logits = tf.transpose(Z3)
    labels = tf.transpose(Y)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels))
    return cost

def random_mini_batches(X, Y, minibatch_size, num_minibatches):
    Xlist = []
    Ylist = []
    for x in range(num_minibatches):
        ind1 = x * minibatch_size
        ind2 = (x+1) * minibatch_size
        Xlist.append(X[ind1:ind2, :])
        Ylist.append(Y[ind1:ind2, :])
    return Xlist, Ylist

def model(netsize, X_train, Y_train, X_test, Y_test, learning_rate, num_epochs, minibatch_size, print_cost=True):
    ops.reset_default_graph()
    (n_x, m) = X_train.shape
    n_y = Y_train.shape[0]
    costs = []
    X, Y = create_placeholders(n_x, n_y)
    parameters = initialize_parameters(netsize[0],netsize[1],netsize[2],netsize[3])
    Z3 = forward_propagation(X, parameters)
    cost = compute_cost(Z3, Y)
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        for epoch in range(num_epochs):
            epoch_cost = 0.
            num_minibatches = int(m / minibatch_size)
            xlist, ylist = random_mini_batches(X_train, Y_train, minibatch_size, num_minibatches)

            for x in range(num_minibatches):
                _, minibatch_cost = sess.run([optimizer, cost], feed_dict={X: xlist[x], Y: ylist[x]})
                epoch_cost += minibatch_cost / num_minibatches

            # for minibatch in minibatches:
            #     (minibatch_X, minibatch_Y) = minibatch
            #     _, minibatch_cost = sess.run([optimizer, cost], feed_dict={X: minibatch_X, Y: minibatch_Y})
            #     epoch_cost += minibatch_cost / num_minibatches

            if print_cost == True and epoch % 20 == 0:
                print("Cost after epoch %i: %f" % (epoch, epoch_cost))
            if print_cost == True and epoch % 5 == 0:
                costs.append(epoch_cost)

        parameters = sess.run(parameters)
        correct_prediction = tf.equal(tf.argmax(Z3), tf.argmax(Y))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print("Train Accuracy:", accuracy.eval({X: X_train, Y: Y_train}))
        print("Test Accuracy:", accuracy.eval({X: X_test, Y: Y_test}))
        return parameters

#import dataset
dataset = np.loadtxt("pima-indians-diabetes.csv", delimiter=",")
X = dataset[:, 0:8]
Y = dataset[:, 8:]
learning_rate = 0.001
num_epochs = 100
batchSize = 10

#declare size of inputs, hidden layers, and outputs
inp = 10
hid1 = 20
hid2 = 20
out = 1
netsize = [inp, hid1, hid2, out]

parameters = model(netsize, X, Y, X, Y, learning_rate, num_epochs, batchSize)
