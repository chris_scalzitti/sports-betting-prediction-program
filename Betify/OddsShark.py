from bs4 import BeautifulSoup as Soup
from urllib.request import urlopen as uReq
import re
import time
from random import randint


def Oddsint(list):
    del list[0]
    for i in range(len(list)):
        if(list[i] == ''): # zero number
            pos = -9999
        else: #negative number
            pos = int(list[i])
        list[i] = pos

    return list

class GameBet:
    def __init__(self,date,gametime,team_home,team_away,odds):
        self.date = date
        self.gamtime = gametime
        self.team_home = team_home
        self.team_away = team_away
        self.odds = odds
    def PrintGame(self):
        print("Date: " + str(self.date) + " Team_Home: " + str(self.team_home) + " Team_Away: " + str(self.team_away) + "Odds " + str(self.odds))


def OddsSharkScraper():
    urls = ['https://www.oddsshark.com/nhl/odds']

    TeamAwayOdds = []
    TeamHomeOdds = []
    affiliate = []
    GamesList = []
    #HomeTeam -- Bottom Team
    #AwayTeam -- Top Team
    for u in urls:
        my_url = u
        sport = u.split('/')
        sport = sport[3]
        print(sport)

        uClient= uReq(my_url)
        page_html=uClient.read()
        uClient.close()

        page_soup = Soup(page_html, "html.parser")
        Top_level = page_soup.body.findAll("div", {"class":"op-content-wrapper"})
        Rows = page_soup.body.findAll("div", {"class":"op-item-row-wrapper not-futures"})

        Teams = page_soup.body.findAll("div", {"class":"op-matchup-right"})
        Matchup_top= page_soup.body.findAll("div", {"class":"op-matchup-team op-matchup-text op-team-top"})
        Matchup_bottom= page_soup.body.findAll("div", {"class":"op-matchup-team op-matchup-text op-team-bottom"})


        ####### Gather every odd row by row including team name date betting site and odds
        #grabbing date for game
        top = Top_level[0]
        date = top.div.div.div["data-op-date"]
        count = 0
        #This is for every game
        for team in Teams:
            row = Rows[count] #going by rows across
            for r in row:
                # grabbing each team
                AwayTeam = Matchup_top[count]["data-op-name"]
                AwayTeam = AwayTeam.split('"')
                AwayTeam = AwayTeam[3]
                HomeTeam = Matchup_bottom[count]["data-op-name"]
                HomeTeam = HomeTeam.split('"')
                HomeTeam = HomeTeam[3]
                for c in r.find_all('div', re.compile("op-item op-spread border-bottom op-.*")):
                    site = c["class"][3] #put site into list
                    site = site[3:len(site)]
                    affiliate.append(site)
                    find = re.findall(r"\"fullgame\":.\d\d\d\"", c["data-op-moneyline"])
                    find = str(find)
                    num = find[14:18]

                    TeamAwayOdds.append(num)
                for c in r.find_all('div', re.compile("op-item op-spread op-.*")):
                    #dont need assign site already done above
                    find = re.findall(r"\"fullgame\":\".\d\d\d\"", c["data-op-moneyline"])
                    find = str(find)
                    num = find[14:18]
                    TeamHomeOdds.append(num)



            del affiliate[0]
            Oddsint(TeamHomeOdds)
            Oddsint(TeamAwayOdds)
            b=GameBet(HomeTeam,AwayTeam,TeamHomeOdds.copy(),TeamAwayOdds.copy(),affiliate.copy())
            b.PrintGame()
            GamesList.append(GameBet(HomeTeam,AwayTeam,TeamHomeOdds.copy(),TeamAwayOdds.copy(),affiliate.copy()))
            TeamHomeOdds.clear()
            TeamAwayOdds.clear()
            affiliate.clear()
            count= count + 1
############# Gather every odd row by row including team name date betting site and odds

        t = randint(5,10)
        print("Time Between Sites")
        print(t)
        time.sleep(t)
    return GamesList

a= OddsSharkScraper()
a[0]




#
# def Compile(data1, data2):
#     total = []
#     total = data1.copy()
#
#     for i in range(len(data1)):
#         if(data1.HomeTeam == data2.HomeTeam and data1.AwayTeam == data2.AwayTeam):
#             for k in range(len(total.site)):
#                 if data.site[k] == total.site[k]
#                     total.odds.Home[k] = data
#