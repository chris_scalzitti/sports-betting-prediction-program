from django.apps import AppConfig


class MLBConfig(AppConfig):
    name = 'MLB'
