from django.shortcuts import render
from .models import MLB_Games


def MLB(request):
    context = {'games' : MLB_Games.objects.all()}  #Games.all is python   #game is html

    return render(request, 'MLB/indexMLB.html', context)
