from django.db import models
from django.utils import timezone


# Create your models here.

class NHL_Games(models.Model):
    class Meta:
        db_table='NHL_Games'


    id = models.AutoField(primary_key=True)
    Home_Team = models.CharField(max_length = 3)
    Away_Team = models.CharField(max_length=3)
    Home_Odd = models.CharField(max_length = 5)
    Away_Odd= models.CharField(max_length=5)
    Event_Date = models.CharField( max_length=20)
    Home_Prediction = models.CharField(max_length = 1000)
    Away_Prediction = models.CharField(max_length = 1000)
    Home_Bet_Perc = models.CharField(max_length=3)
    Away_Bet_Perc = models.CharField(max_length=3)
    Home_Betting_Site = models.CharField(max_length = 1000)
    Away_Betting_Site = models.CharField(max_length = 1000)

    def __str__(self):
        return self.Home_Team + " vs " + self.Away_Team



