from django.shortcuts import render
from .models import NHL_Games


def NHL(request):
    context = {'games' :  NHL_Games.objects.all()}  #Games.all is python   #game is html
    return render(request, 'NHL/indexNHL.html', context)

def detail(request):
    return render(request,'NHL/detailNHL.html' )

