from django.apps import AppConfig


class NHLConfig(AppConfig):
    name = 'NHL'
