from django.shortcuts import render
from .models import GameNBA


def NBA(request):
    context = {'games' : GameNBA.objects.all()}  #Games.all is python   #game is html

    return render(request, 'NBA/indexNBA.html', context)
