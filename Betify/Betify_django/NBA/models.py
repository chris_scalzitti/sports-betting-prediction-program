from django.db import models
from django.utils import timezone


# Create your models here.

class GameNBA(models.Model):

    Team_Id_home = models.CharField(max_length = 3)
    Team_Name_home = models.CharField(max_length = 50)
    Team_Id_away = models.CharField(max_length=3)
    Team_Name_away = models.CharField(max_length=50)
    Odds_home = models.CharField(max_length = 5)
    Odds_away = models.CharField(max_length=5)
    Date = models.DateTimeField(default = timezone.now)
    Team_logo_home = models.CharField(max_length = 1000)
    Team_logo_away = models.CharField(max_length = 1000)
    Bet_home = models.CharField(max_length=5)
    Bet_away = models.CharField(max_length=5)
    def __str__(self):
        return self.Team_Name_home + " vs " + self.Team_Name_away



