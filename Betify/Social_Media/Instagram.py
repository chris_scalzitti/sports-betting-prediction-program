import os
#auto poster
from selenium import webdriver
import time
import pyautogui


#image_maker
from PIL import Image,ImageDraw,ImageFont
import random

#dateStamp
import datetime


def Auto_Post(Comment_Text,Path): #posts image on instagram, Path the folder where the image is stored
    #logins into hootsuite and starts a new post
    browser=webdriver.Chrome(r'chromedriver.exe')
    browser.get('https://hootsuite.com/dashboard')
    browser.maximize_window()
    time.sleep(1)
    email=browser.find_element_by_id('loginEmailInput')
    email.send_keys('betify.ca@gmail.com')
    email_password=browser.find_element_by_id('loginPasswordInput')
    email_password.send_keys('Betify777')
    time.sleep(1)
    try:
        Sign_in =browser.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[4]/form[1]/button')
        Sign_in.click()
    except:
        pyautogui.click(1450, 500)

    time.sleep(5)
    New_Post = browser.find_element_by_xpath('//*[@id="streamsHeader"]/div/div/div[2]/div[1]/div/button/div')
    New_Post.click()
    #Clicks the boxes in the create post popup
    time.sleep(2)
    pyautogui.click(412,450)
    time.sleep(1)
    pyautogui.click(412,550)
    time.sleep(1)
    pyautogui.click(412,750)
    pyautogui.typewrite(Comment_Text)

    #file Selection
    Upload = browser.find_element_by_xpath('//*[@id="fullScreenComposerMountPoint"]/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[2]/div/div/div/div/button')
    Upload.click()
    time.sleep(2)
    pyautogui.click(400,60)
    pyautogui.press('backspace')
    pyautogui.typewrite(Path)
    time.sleep(1)
    pyautogui.press('enter')
    time.sleep(1)

    #delete old photo
    pyautogui.click(350, 180)
    pyautogui.press('delete')
    time.sleep(1)

    #select new photo
    pyautogui.click(350, 180, clicks=2)
    time.sleep(2)

    #select post button
    time.sleep(5)
    Post = browser.find_element_by_xpath('//*[@id="fullScreenComposerMountPoint"]/div/div/div/div/div/div[3]/div/button')
    Post.click()


    time.sleep(20)
    browser.quit()


def resizeImg(imgIn, downto):#image is reduced to a percentage of the real image
    img = imgIn
    size = width, height = img.size
    finalw =int(width * (downto/width))
    finalh = int(height* (downto/height))
    img = img.resize((finalw,finalh))
    return img

def Overlay(baseimg,sportimg,homeimg,awayimg,Hodds,Aodds):# overlays base, teams and sport logo
     #assign and getting sizes
    base = baseimg
    width, height = base.size
    home = homeimg
    hwidth , hheigt = home.size
    away = awayimg
    awidth, aheigt = away.size
    sport = sportimg
    swidth, sheight = sport.size
    sMiddle = int(((width - 930 - hwidth) - (swidth / 2)))

    #positioning
    homepos = (100, 300)
    awaypos = (width - 500, 300)
    sportpos = (sMiddle, 30)
    Gtextpos = (sMiddle +150,950)
    Dtextpos = (sMiddle, 15)
    htextpos= (100 + (hwidth/4), hheigt +300)
    atextpos = ((width - 500+(awidth/4)), aheigt +300)

    #combining
    new_img = Image.new('RGBA', (width, height), (0, 0, 0, 0))
    new_img.paste(base, (0, 0))
    #new_img.paste(sport, sportpos, mask=sport)
    new_img.paste(home, homepos, mask=home)
    new_img.paste(away, awaypos, mask=away)

     #Text
    color =(40,50,60)
    font = ImageFont.truetype('fonts/Helvetica-Bold.ttf', size=190)
    font_small = ImageFont.truetype('fonts/Helvetica-Oblique.ttf', size=130)
    draw = ImageDraw.Draw(new_img)

    draw.text(Dtextpos, text=str(datetime.date.today()), fill=color, font=font)
    draw.text((Gtextpos[0],160), text="Predictions", fill=color, font=font_small)
    draw.text(Gtextpos,text="Game Time",fill=color, font=font_small)
    draw.text(htextpos, text=Hodds,fill=color, font=font)
    draw.text(atextpos, text=Aodds,fill=color, font=font)

    return new_img



def Make_Photo(Sport,stockId, HomeTeam, AwayTeam, Home_odds, Away_odds,Final_Path):
    #determine which sport
    if Sport == "NHL":
        stock_path = (r"NHL_Stock_Photos")
        stock_path = stock_path+"\\"+ stockId +".png"
        team_paths = (r"..\Betify_django\home\static\home\assets\img\NHL_Team_Logos")
    elif Sport == "MLB":
        stock_path = (r"MLB_Stock_Photos")
        stock_path = stock_path + "\\" + stockId + ".png"
        team_paths = (r"..\Betify_django\home\static\home\assets\img\MLB_Team_Logos")
    elif Sport == "NBA":
        stock_path = (r"NBA_Stock_Photos")
        stock_path = stock_path + "\\" + stockId + ".png"
        team_paths = (r"..\Betify_django\home\static\home\assets\img\NBA_Team_Logos")

    #get sport picture
    sportpath = team_paths + "\\"+ Sport + ".png"
    sportpic= Image.open(sportpath,'r').convert("RGBA")
    sportpic= resizeImg(sportpic,200)

    # get team pictures
    homepath = team_paths + "\\"+ HomeTeam + ".png"
    homepic = Image.open(homepath).convert("RGBA")


    awaypath = team_paths + "\\" + AwayTeam + ".png"
    awaypic = Image.open(awaypath,'r').convert("RGBA")

    # get base image
    base = Image.open(stock_path, 'r').convert("RGBA")


    #resize sport home and away team images
    sportpic = resizeImg(sportpic,150)
    homepic =  resizeImg(homepic,400)
    awaypic = resizeImg( awaypic,400)

    new_img = Overlay(base,sportpic,homepic,awaypic,Home_odds,Away_odds)

    #save File
    date = datetime.date.today()
    Post_Img = Final_Path + "\\" + str(date) + str(random.randint(1,100)) + '.png'
    new_img.save(Post_Img)


def Make_Comment(Home_Team, Away_Team, HomePredict, AwayPredict):
    First_Choice = ["Big Game between ", "Huge matchup today between ", "Today has another meeting between "]
    Teams = str(Home_Team + " and " + Away_Team + ".")
    Second_Choice = [" This will be a hard fought battle "," The "+Home_Team+" make a pit stop at home to battle the " + Away_Team +" "," These teams will face-Off "]
    Third_Choice = ["with " + Home_Team + " having a " + HomePredict + " chance of winning, and "  + Away_Team + " having a " + AwayPredict + " of taking the game."]
    Fourth_Choice  = [" Checkout www.betify.ca for more information and which book maker has the best odds!"]
    ran1 = random.randint(0,2)
    ran2 = random.randint(0,2)
    comment = str(First_Choice[ran1]) + Teams + str(Second_Choice[ran2]) + Third_Choice[0] + Fourth_Choice[0]

    return comment

def Instagram (Sport, StockPhotoID, Home_Team, Away_Team,HomePredict,AwayPredict):
    #path of compiled photo
    Final_path = (r'C:\Users\antsc\Documents\Anthony\sports-betting-prediction-program\Betify\Social_Media' + '\\' + str(Sport) +'_Upload')


    #make the comment for the photo
    Comment=Make_Comment(Home_Team, Away_Team, HomePredict, AwayPredict)

    #compile the photo
    Make_Photo(Sport, StockPhotoID, Home_Team, Away_Team, HomePredict, AwayPredict,Final_path)



    # posting the image to instagram
    Auto_Post(Comment, Final_path)



# Making an image from inputs
Sport = "NHL"
StockPhotoID = Sport
Home_Team = "CAR"
Away_Team = "BOS"
HomePredict = "50%"
AwayPredict = "50%"

Instagram(Sport, StockPhotoID, Home_Team, Away_Team,HomePredict,AwayPredict)
print("Done")
