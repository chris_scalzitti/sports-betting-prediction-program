from ohmysportsfeedspy import MySportsFeeds
import sqlite3
import time
from Common import NHL_Dict
from Common import NHL_Abbr_Conversion
from NHL_NN import predict_Game
import Bets
import requests


# ---- Daily Games
import datetime



# ----------- Get Todays Game Schedule -------------------
def getNHLDailyGames():
    Data_query = MySportsFeeds('2.0')
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    Games = []
    date = str(datetime.date.today())
    date = date.replace("-", "")

    Output = Data_query.msf_get_data(league='nhl', season='current', feed='daily_games', date=str(date),
                                     format='json', force='true')

    if not Output['games']:
        Output = Data_query.msf_get_data(league='nhl', season='2018-2019-playoff', feed='daily_games', date=str(date),
                                         format='json', force='true')
        print(Output)
    for i in range(len(Output['games'])):
        Game = [Output['games'][i]['schedule']['homeTeam']['abbreviation'],
                Output['games'][i]['schedule']['awayTeam']['abbreviation']]
        Games.append(Game)

    return Games
    # ------------ Get Odds for Particular game ----------------


def getNHLOdds():
    response = requests.get(
        "https://therundown-therundown-v1.p.rapidapi.com/sports/6/events?include=all_periods%2C+scores%2C+and%2For+teams",
        headers={
            "X-RapidAPI-Host": "therundown-therundown-v1.p.rapidapi.com",
            "X-RapidAPI-Key": "F4LcPM2OFgmshE8aFxbcOTw4AdfNp1hBHWfjsnhbZQlsFtVpFy"
        }
    )
    response = response.json()
    print(response)
    data = []
    game = []
    odds = []
    time_pulled = str(datetime.datetime.now())
    for event in response['events']:
        game.clear()
        game.append(time_pulled)
        game.append((str(event['event_date']).replace('T', ' ').replace('Z', '')))
        # Getting Home and Away Teams
        if event['teams_normalized'][0]['is_home']:

            home = event['teams_normalized'][0]['abbreviation']
            away = event['teams_normalized'][1]['abbreviation']
        else:
            home = event['teams_normalized'][1]['abbreviation']
            away = event['teams_normalized'][0]['abbreviation']

        if home in NHL_Abbr_Conversion:
            home = NHL_Abbr_Conversion[home]
        if away in NHL_Abbr_Conversion:
            away = NHL_Abbr_Conversion[away]
        game.append(home)
        game.append(away)

        for line in event["lines"]:
            odds.clear()
            odds.append(str(event["lines"][line]['moneyline']['date_updated']).replace('T', ' ').replace('Z', ''))
            odds.append(event["lines"][line]['affiliate']['affiliate_name'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['moneyline']['moneyline_home']))
            odds.append(Bets.convertAmericanDec(event["lines"][line]['moneyline']['moneyline_away']))
            odds.append(event["lines"][line]['spread']['point_spread_home'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['spread']['point_spread_home_money']))
            odds.append(event["lines"][line]['spread']['point_spread_away'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['spread']['point_spread_away_money']))
            data.append(game + odds)

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    sql = '''INSERT into NHL_Odds(Time_Pulled, Event_Date, Home_Team, Away_Team,Date_Updated,Betting_Site,Home_Odds,Away_Odds,Puckline_Home,Puckline_Home_Odds,Puckline_Away,Puckline_Away_Odds) values(?,?,?,?,?,?,?,?,?,?,?,?) '''
    cur.executemany(sql, data)
    connection.commit()
    connection.close()

    print(data)

# ----------- Get Todays Game Schedule -------------------

def NHL_Website():
    getNHLOdds()
    home_odds_index = 6
    away_odds_index = 7
    home_puckline_index = 8
    away_puckline_index = 10
    home_puckline_odds_index = 9
    away_puckline_odds_index = 11

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()

    web_conn = sqlite3.connect('../Betify/Betify_django/db.sqlite3')
    web_cur = web_conn.cursor()
    sql = '''delete from NHL_Games'''
    web_cur.execute(sql)
    sql = '''SELECT distinct Event_Date, Home_Team, Away_Team 
    from (select * from NHL_Odds 
    where Time_Pulled = (select max(Time_Pulled) from NHL_Odds))'''
    cur.execute(sql)
    event = cur.fetchall()

    data = []
    home_odds = []
    away_odds = []
    web_info = []

    if not event:
        print("No Events")
        exit(code=-1)

    for i in range(len(event)):
        home_odds.clear()
        away_odds.clear()
        web_info.clear()


        sql = '''SELECT * from NHL_Odds 
        where Event_Date = ''' + "'" + event[i][0] + "'" + '''
        and Betting_Site = 'Pinnacle' 
        and Home_Team= ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team= ''' + "'" + event[i][2] + "'" + '''
        order by Time_Pulled'''
        cur.execute(sql)
        odds_stats = cur.fetchall()

        sql = '''SELECT max (Home_Odds)as Max_Odds,Betting_Site
        from NHL_Odds
        where Home_Team = ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team =''' + "'" + event[i][2] + "'" + ''' 
        and Time_Pulled = (select max(Time_Pulled) from NHL_Odds)
        union
        select max(Away_Odds),Betting_Site from NHL_Odds 
        where Home_Team = ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team = ''' + "'" + event[i][2] + "'" + ''' 
        and Time_Pulled = (select max(Time_Pulled) from NHL_Odds)'''
        cur.execute(sql)
        max_odds = cur.fetchall()

        home_odds.append(odds_stats[0][home_odds_index])
        home_odds.append(odds_stats[-1][home_odds_index])
        home_odds.append(odds_stats[-1][home_puckline_index])
        home_odds.append(odds_stats[-1][home_puckline_odds_index])

        away_odds.append(odds_stats[0][away_odds_index])
        away_odds.append(odds_stats[-1][away_odds_index])
        away_odds.append(odds_stats[-1][away_puckline_index])
        away_odds.append(odds_stats[-1][away_puckline_odds_index])


        prediction = predict_Game(home_team=event[i][1],away_team=event[i][2],home_odds=home_odds,away_odds=away_odds)

        print(prediction)
        home_bet_perc = Bets.getBetAmount(bank_roll=1, odds=max_odds[0][0],
                                          prediction=prediction)
        away_bet_perc = Bets.getBetAmount(bank_roll=1, odds=max_odds[1][0],
                                          prediction=(1-prediction))
        #Event Date, Home Team, Away Team
        web_info.append(event[i][0])
        web_info.append(event[i][1])
        web_info.append(event[i][2])
        #Home Team
        web_info.append(prediction)
        web_info.append(max_odds[0][0])
        web_info.append(home_bet_perc)
        web_info.append(max_odds[0][1])
        # Away Team
        web_info.append((1-prediction))
        web_info.append(max_odds[1][0])
        web_info.append(away_bet_perc)
        web_info.append(max_odds[1][1])
        print(web_info)

        sql = '''insert into NHL_Games(Event_Date,Home_Team,Away_Team,Home_Prediction,Home_Odd,Home_Bet_Perc,Home_Betting_Site,
        Away_Prediction,Away_Odd,Away_Bet_Perc,Away_Betting_Site) values(?,?,?,?,?,?,?,?,?,?,?)'''
        web_cur.execute(sql, web_info)
    web_conn.commit()
    web_conn.close()
    connection.close()




# ------------ Get Season Game Logs -----------------------
# Puts data for the season in database and cleans all data
def seasonGameLogNHL(low_year, high_year, season_type):
    Data_query = MySportsFeeds('2.0')
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    data = []

    for team in range(1, 32):

        for year in range(low_year, high_year):
            print("getting " + NHL_Dict[team] + " year " + str(year))
            season = Data_query.msf_get_data(league='NHL', season=str(year) + '-' + season_type,
                                             feed='seasonal_team_gamelogs', team=NHL_Dict[team], format='json',
                                             force='true')
            data.append(season)
            time.sleep(10)

    if (NHL_Dict[team] == 15):
        time.sleep(330)
    team = []
    stats = []
    insert = []

    for season in data:
        gamesplayed = 0
        winstreak = 0
        insert.clear()
        for game in season['gamelogs']:
            avg = 0
            stats.clear()
            team.clear()

            if not game:
                continue
            if 'shootoutWins' in game['stats']['standings']:
                if game['stats']['standings']['shootoutWins'] == 1:
                    game['stats']['standings']['wins'] = 1
                del game['stats']['standings']['shootoutWins']

            if 'shootoutLosses' in game['stats']['standings']:
                if game['stats']['standings']['shootoutLosses'] == 1:
                    game['stats']['standings']['losses'] = 1
                del game['stats']['standings']['shootoutLosses']

            if 'rows' in game['stats']['standings']:
                del game['stats']['standings']['rows']
            if 'blockedShots' in game['stats']['miscellaneous']:
                del game['stats']['miscellaneous']['blockedShots']

            if game['stats']['standings']['overtimeWins'] == 1:
                game['stats']['standings']['wins'] = 1
            if game['stats']['standings']['overtimeLosses'] == 1:
                game['stats']['standings']['losses'] = 1

            if game['game']['homeTeamAbbreviation'] == game['team']['abbreviation']:
                home = 1
            else:
                home = 0

            if gamesplayed > 0:
                if gamesplayed >= 5:
                    for i in range(gamesplayed - 5, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / 5
                else:
                    for i in range(0, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / gamesplayed

                if season['gamelogs'][gamesplayed - 1]['stats']['standings']['wins'] == 1:
                    winstreak = winstreak + 1
                else:
                    winstreak = 0

            team.append(game['game']['id'])
            team.append(game['game']['startTime'].split("T")[0])
            team.append(game['game']['awayTeamAbbreviation'])
            team.append(game['game']['homeTeamAbbreviation'])
            team.append(game['team']['abbreviation'])
            stats = list(game['stats']['standings'].values()) + list(game['stats']['faceoffs'].values()) + list(
                game['stats']['powerplay'].values()) + list(game['stats']['miscellaneous'].values())

            stats.append(home)
            stats.append(avg)
            stats.append(winstreak)
            insert.append(team + stats)
            gamesplayed = gamesplayed + 1

        sql = '''INSERT or ignore into NHL_Team_Stats_Regular(
        Game_ID,
        Game_Date,
        Away_Team_Abbv,
        Home_Team_Abbv,
        Team,
        Wins,
        Losses,
        Overtime_Wins,
        Overtime_Losses,
        Points,
        Faceoff_Wins,
        Faceoff_Losses,
        Faceoff_Percent,
        Powerplays,
        Powerplay_Goals,
        Powerplay_Percent,
        Penalty_Kills,
        Penalty_Goals_Allowed,
        Penalty_Kill_Percent,
        Shorthanded_Goals_For,
        Shorthanded_Goals_Against,
        Goals_For,
        Goals_Against,
        Shots,
        Penalties,
        Penalty_Minutes,
        Hits,
        Home_Or_Away,
        Five_Game_Avg,
        Win_Streak) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'''
        connection = sqlite3.connect('../Database/sports_data.db')
        cur = connection.cursor()
        cur.executemany(sql, insert)
        connection.commit()
    connection.close()

# update test set ShotsA = (select ShotsF from test where team = 'BOS') where Game_ID = (Select Game_ID from test where Team = 'BOS') and Team != 'BOS'
#update test set savep = (select (ShotsA-GA)/cast(ShotsA as real) from test;
#select (ShotsA-GA)/cast(ShotsA as real) from test
#goals +-
#save percentage
#shooting percentage
