import csv
import sqlite3
import os.path
from time import strptime
import json


#######################################################
def read_NHL_csv(cur):
    season = []
    for filename in os.listdir('NHL_CSV'):
        if filename.endswith(".csv"):
            with open('NHL_CSV/'+ filename) as csvfile:
                readCSV = csv.reader(csvfile, delimiter=',', quotechar='"')
                for row in readCSV:
                    if len(row)== 1:
                        data = str(row[0])
                        game = data.replace("'","").replace("]","").replace("[","").split(",")
                        game.pop(0)  # Removing Date Updated
                        season.append(game)


        #Clean Stats
        remove = [' #BlockedShots', ' #ShootoutLosses',' #ShootoutWins', ' #Rows']
        for stat in remove:
            if stat in season[0]:
                index = season[0].index(stat)
                for row in season:
                    row.pop(index)

        season.pop(0)

        if len(season) != 0:
            cur.executemany(
            '''INSERT INTO NHL_Team_Stats(
            Game_ID,
            Game_Date,
            Game_Time,
            Away_Team_ID,
            Away_Team_Abbr,
            Away_Team_City,
            Away_Team_Name,
            Home_Team_ID,
            Home_Team_Abbr,
            Home_Team_City,
            Home_Team_Name,
            Venue_ID,
            Venue_Name,
            Team_ID,
            Team_Abbr,
            Team_City,
            Team_Name,
            Wins,
            Losses,
            Overtimewins,
            OvertimeLosses,
            Points,
            FaceoffWins,
            FaceoffLosses,
            FaceoffPercent,
            Powerplays,
            PowerplayGoals,
            PowerplayPercent,
            PenaltyKills,
            PenaltyKillGoalsAllowed,
            PenaltyKillPercent,
            ShorthandedGoalsFor,
            ShorthandedGoalsAgainst,
            GoalsFor,
            GoalsAgainst,
            Shots,
            Penalties,
            PenaltyMinutes,
            Hits)
            VALUES
            (?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?)''', season)
        season.clear()

# ###################################################################
# Odds csv to json
# x = (year-2000)+1
#     wb = xlrd.open_workbook('Odds_History/nhl odds '+str(year)+'-'+str(x)+'.xlsx')
#     sh = wb.sheet_by_index(0)
#     # List to hold dictionaries
#     data_list.clear()
#     json_list.clear()
#     # Iterate through each row in worksheet and fetch values into dict
#     for rownum in range(1, sh.nrows):
#
#         data = OrderedDict()
#         row_values = sh.row_values(rownum)
#         date = str(row_values[0])
#
#         if len(date)>5:
#             date = str(year)+"-" +date[0:2]+"-"+date[2:4]
#         else:
#             date = str(year+1) + "-0" + date[0:1] + "-" + date[1:3]
#
#         moneyline_o = row_values[8]
#         moneyline_c = row_values[9]
#         if moneyline_c=="NL":
#             moneyline_c = 100
#         if moneyline_o=="NL":
#             moneyline_o = 100
#         print(row_values[10])
#         puckline = float(row_values[10].split("(")[0].replace('+',''))
#         puckline_odds = float(row_values[10].split("(")[1].split(")")[0].replace('+',''))
#         home_or_away = row_values[2]
#         if home_or_away == 'H':
#             home_or_away = 1
#         else:
#             home_or_away =0
#
#         data['Date'] =date
#         data['Home_or_Away'] = home_or_away
#         data["Team"] = NHL_Name_Abbr[row_values[3]]
#         data['Moneyline_Open'] = Odds.convertAmericanDec(moneyline_o)
#         data['Moneyline_Close'] = Odds.convertAmericanDec(moneyline_c)
#         data['Puckline'] = puckline
#         data['Puckline_Odd'] = Odds.convertAmericanDec(puckline_odds)
#         data_list.append(data)
#
#     json_list = {'Data': data_list} # Added line
#     # Serialize the list of dicts to JSON
#     j = json.dumps(json_list)
#
#     # Write to file
#     with open('Odds_History/nhl_odds_'+str(year)+'-'+str(year+1)+'.json', 'w') as f:
#         f.write(j)
# for year in range(2014,2019):
#
#     with open('Odds_History/nhl_odds_'+str(year)+'-'+str(year+1)+'.json', 'r') as f:
#         datastore = json.load(f)
#
#     for game in datastore['Data']:
#         date = datetime.datetime.strptime(game['Date'], "%Y-%m-%d")
#         date_after = date + datetime.timedelta(days=1)
#         sql = '''Update NHL_Team_Stats_Regular set
#         Moneyline_Open ='''+str(game['Moneyline_Open'])+'''
#         ,Moneyline_Close='''+str(game['Moneyline_Close'])+'''
#         ,Puckline='''+str(game['Puckline'])+'''
#         ,Puckline_Odds='''+str(game['Puckline_Odd'])+'''
#         where Game_Date = ''' + "'" +game['Date'] + "'" + '''
#         and Team='''+"'"+str(game['Team'])+"'"'''
#         and Home_Or_Away='''+str(game['Home_or_Away'])
#         print(sql)
#         cur.execute(sql)
#     connection.commit()
# connection.close()
################################################################################################
# Entering Pinnical Odds Data
# with open("Odds_NHL.json", 'r') as f:
#      datastore = json.load(f)
# data = datastore[2]['data']
# connection = sqlite3.connect('../Database/sports_data.db')
# cur = connection.cursor()
# for game in data:
#
#     month = str(strptime(game["month"], '%b').tm_mon)
#     day = game["day"]
#
#     if(int(day)<10):
#         day = "0"+day
#     if (int(month) < 10):
#         month = "0" + month
#
#     date = game["year"]+"-"+month+"-"+day
#     print(date)
#     home = game["teamID_home"]
#     away = game["teamID_away"]
#     open_home = game["openingHome"]
#     open_away = game["openingAway"]
#     close_home = game["closingHome"]
#     close_away = game["closingAway"]
#
#
#     if (open_home or open_away or close_home or close_away):
#
#         sql_home='''Update NHL_Team_Stats_Regular set
#              Moneyline_Open =''' + open_home + ''',
#              Moneyline_Close=''' + close_home + '''
#              where Game_Date = ''' + "'" + date + "'" + '''
#              and Team=''' + "'" + home + "'"
#         sql_away = '''Update NHL_Team_Stats_Regular set
#                     Moneyline_Open =''' + open_away + ''',
#                     Moneyline_Close=''' + close_away + '''
#                     where Game_Date = ''' + "'" + date + "'" + '''
#                     and Team=''' + "'" + away + "'"
#
#
#
#
#         cur.execute(sql_home)
#         cur.execute(sql_away)
# connection.commit()
# connection.close()
