from ohmysportsfeedspy import MySportsFeeds
import sqlite3
import time
from Common import MLB_Dict
from Common import MLB_Conv
from Betting import Bets
import requests
from Machine_Learning import MLB_NN

# ---- Daily Games
import datetime

# ----------- Get Todays Game Schedule -------------------
def getMLBDailyGames():
    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    Games = []
    date = str(datetime.date.today())
    date = date.replace("-", "")
    Output = Data_query.msf_get_data(league='mlb', season='current', feed='daily_games', date=str(date),
                                     format='json', force='true')

    if not Output['games']:
        Output = Data_query.msf_get_data(league='mlb', season='2018-2019-playoff', feed='daily_games', date=str(date),
                                         format='json', force='true')
        print(Output)
    for i in range(len(Output['games'])):
        Game = [Output['games'][i]['schedule']['homeTeam']['abbreviation'],
                Output['games'][i]['schedule']['awayTeam']['abbreviation']]
        Games.append(Game)

    return Games

def getMLBOdds():
    response = requests.get(
        "https://therundown-therundown-v1.p.rapidapi.com/sports/3/events?include=all_periods%2C+scores%2C+and%2For+teams",
        headers={
            "X-RapidAPI-Host": "therundown-therundown-v1.p.rapidapi.com",
            "X-RapidAPI-Key": "F4LcPM2OFgmshE8aFxbcOTw4AdfNp1hBHWfjsnhbZQlsFtVpFy"
        }
    )
    response = response.json()

    data = []
    game = []
    odds = []
    time_pulled = str(datetime.datetime.now())
    for event in response['events']:
        game.clear()
        game.append(time_pulled)
        game.append((str(event['event_date']).replace('T', ' ').replace('Z', '')))
        # Getting Home and Away Teams
        if event['teams_normalized'][0]['is_home']:
            home = event['teams_normalized'][0]['abbreviation']
            away = event['teams_normalized'][1]['abbreviation']
        else:
            home = event['teams_normalized'][1]['abbreviation']
            away = event['teams_normalized'][0]['abbreviation']

        if home in MLB_Conv:
            home = MLB_Conv[home]
        if away in MLB_Conv:
            away = MLB_Conv[away]
        game.append(home)
        game.append(away)

        for line in event["lines"]:
            odds.clear()
            odds.append(str(event["lines"][line]['moneyline']['date_updated']).replace('T', ' ').replace('Z', ''))
            odds.append(event["lines"][line]['affiliate']['affiliate_name'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['moneyline']['moneyline_home']))
            odds.append(Bets.convertAmericanDec(event["lines"][line]['moneyline']['moneyline_away']))
            odds.append(event["lines"][line]['spread']['point_spread_home'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['spread']['point_spread_home_money']))
            odds.append(event["lines"][line]['spread']['point_spread_away'])
            odds.append(Bets.convertAmericanDec(event["lines"][line]['spread']['point_spread_away_money']))
            data.append(game + odds)

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()
    sql = '''INSERT into MLB_Odds(Time_Pulled, Event_Date, Home_Team, Away_Team,Date_Updated,Betting_Site,Home_Odds,Away_Odds,Point_Spread_Home,Point_Spread_Home_Odds,Point_Spread_Away,Point_Spread_Away_Odds) values(?,?,?,?,?,?,?,?,?,?,?,?) '''
    cur.executemany(sql, data)
    connection.commit()
    connection.close()

    print(data)
def MLB_Website():
    getMLBOdds()
    home_odds_index = 6
    away_odds_index = 7
    home_spread_index = 8
    away_spread_index = 10
    home_spread_odds_index = 9
    away_spread_odds_index = 11

    connection = sqlite3.connect('../Database/sports_data.db')
    cur = connection.cursor()

    web_conn = sqlite3.connect('../Betify/Betify_django/db.sqlite3')
    web_cur = web_conn.cursor()
    sql = '''delete from MLB_Games'''
    web_cur.execute(sql)
    sql = '''SELECT distinct Event_Date, Home_Team, Away_Team 
    from (select * from MLB_Odds 
    where Time_Pulled = (select max(Time_Pulled) from MLB_Odds))'''
    cur.execute(sql)
    event = cur.fetchall()


    home_odds = []
    away_odds = []
    web_info = []

    if not event:
        print("No Events")
        exit(code=-1)

    for i in range(len(event)):
        home_odds.clear()
        away_odds.clear()
        web_info.clear()


        sql = '''SELECT * from MLB_Odds 
        where Event_Date = ''' + "'" + event[i][0] + "'" + '''
        and Betting_Site = 'Pinnacle' 
        and Home_Team= ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team= ''' + "'" + event[i][2] + "'" + '''
        order by Time_Pulled'''
        cur.execute(sql)
        odds_stats = cur.fetchall()

        sql = '''SELECT max (Home_Odds)as Max_Odds,Betting_Site
        from MLB_Odds
        where Home_Team = ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team =''' + "'" + event[i][2] + "'" + ''' 
        and Time_Pulled = (select max(Time_Pulled) from MLB_Odds)
        union
        select max(Away_Odds),Betting_Site from MLB_Odds 
        where Home_Team = ''' + "'" + event[i][1] + "'" + ''' 
        and Away_Team = ''' + "'" + event[i][2] + "'" + ''' 
        and Time_Pulled = (select max(Time_Pulled) from MLB_Odds)'''
        cur.execute(sql)
        max_odds = cur.fetchall()

        # For prediction
        # home_odds.append(odds_stats[0][home_odds_index])
        # home_odds.append(odds_stats[-1][home_spread_index])
        # home_odds.append(odds_stats[-1][home_spread_odds_index])
        #
        # away_odds.append(odds_stats[0][away_odds_index])
        # away_odds.append(odds_stats[-1][away_odds_index])
        # away_odds.append(odds_stats[-1][away_spread_index])
        # away_odds.append(odds_stats[-1][away_spread_odds_index])


        prediction = MLB_NN.predict_MLB_Game(home_team=event[i][1],away_team= event[i][2])


        home_bet_perc = Bets.getBetAmount(bank_roll=1, odds=max_odds[0][0],
                                          prediction=prediction[0,0])
        away_bet_perc = Bets.getBetAmount(bank_roll=1, odds=max_odds[1][0],
                                          prediction=prediction[0,1])
        #Event Date, Home Team, Away Team
        web_info.append(event[i][0])
        web_info.append(event[i][1])
        web_info.append(event[i][2])
        #Home Team
        web_info.append(prediction[0,0])
        web_info.append(max_odds[0][0])
        web_info.append(home_bet_perc)
        web_info.append(max_odds[0][1])
        # Away Team
        web_info.append((prediction[0,1]))
        web_info.append(max_odds[1][0])
        web_info.append(away_bet_perc)
        web_info.append(max_odds[1][1])
        print(web_info)

        sql = '''insert into MLB_Games(Event_Date,Home_Team,Away_Team,Home_Prediction,Home_Odd,Home_Bet_Perc,Home_Betting_Site,
        Away_Prediction,Away_Odd,Away_Bet_Perc,Away_Betting_Site) values(?,?,?,?,?,?,?,?,?,?,?)'''
        web_cur.execute(sql, web_info)
    web_conn.commit()
    web_conn.close()
    connection.close()

def getMLBColums(year, team): #must use season above 2016
    CreateDataBaseString= 'CREATE TABLE IF NOT EXISTS MLB_Team_Stats_Regular(Game_ID NUMERIC,Game_Date DATE, Away_Team_Abbv TEXT, Home_Team_Abbv TEXT, Team TEXT,'
    InsertDataBaseString = 'INSERT or ignore into MLB_Team_Stats_Regular(Game_ID,Game_Date, Away_Team_Abbv,Home_Team_Abbv, Team,'


    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')
    print(" IN GET MLB COLLUMS " + str(MLB_Dict[team]))

    season = Data_query.msf_get_data(league='MLB', season=str(year) + '-' + 'regular',
                                     feed='seasonal_team_gamelogs', team=MLB_Dict[team], format='json', force='true')


    print(season['gamelogs'])

    game = season['gamelogs'][0]#make sure to get a correct team abbv

    #----Put the colum headers in ---------------------------------
    StatKeys = list(game['stats'].keys()) #returned at bottom function ("batting", "Pitching" "Fielding" etc)
    numOfColums = 5 #init 5 see above collums
    for stat in game['stats']: #for batting, fielding, pitching
        columns = game['stats'][stat].keys() #get all the keys to batting, then fielding, then pithing etc concatinate
        for column in columns:
            CreateDataBaseString = CreateDataBaseString + str(column) + " REAL, "
            InsertDataBaseString = InsertDataBaseString + str(column) + ", "
            numOfColums += 1

    #----get value string  eg VALUES(?,?,?,?,?,? etc) -----------------
    valueString = 'VALUES('

    for i in range(numOfColums + 3 ): #make the VALUES(?,?,?,?,?,? etc) section
        if i == numOfColums + 2:
            valueString =  valueString + "?)"
        else:
            valueString = valueString + "?, "
    print(str(MLB_Dict[team]) +  " " + str(numOfColums))
    #-Clean Up and return -----------------
    CreateDataBaseString = CreateDataBaseString + "Home_Or_Away REAL, Five_Game_Avg REAL, Win_Streak REAL) "
    InsertDataBaseString = InsertDataBaseString + "Home_Or_Away, Five_Game_Avg, Win_Streak) " + valueString

    return season, CreateDataBaseString,InsertDataBaseString, StatKeys


# ------------ Get Season Game Logs -----------------------
# Puts data for the season in database and cleans all data
def seasonGameLogMLB(low_year, high_year, season_type):
    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    data = []

    for team in range(1, 31): #make sure this is the correct number for the teams in the leauge

        for year in range(low_year, high_year+1):
            print("NEW TEAM: " + MLB_Dict[team] + " year " + str(year))
            season, CreateDataBaseString, InsertDataBaseString, OverallStatKeys = getMLBColums(year, team)

            data.append(season)
            time.sleep(10)

    if (MLB_Dict[team] == 15):
        time.sleep(330)
    team = []
    stats = []
    insert = []

    for season in data:
        gamesplayed = 0
        winstreak = 0
        insert.clear()
        for game in season['gamelogs']:
            avg = 0
            stats.clear()
            team.clear()

            if not game:
                continue

            if game['game']['homeTeamAbbreviation'] == game['team']['abbreviation']:
                home = 1
            else:
                home = 0
    #win streak and moving average
            if gamesplayed > 0:
                if gamesplayed >= 5:
                    for i in range(gamesplayed - 5, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / 5
                else:
                    for i in range(0, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / gamesplayed

                if season['gamelogs'][gamesplayed - 1]['stats']['standings']['wins'] == 1:
                    winstreak = winstreak + 1
                else:
                    winstreak = 0

            team.append(game['game']['id'])
            team.append(game['game']['startTime'].split("T")[0])
            team.append(game['game']['awayTeamAbbreviation'])
            team.append(game['game']['homeTeamAbbreviation'])
            team.append(game['team']['abbreviation'])


            stats = list(game['stats'][OverallStatKeys[0]].values()) + list(game['stats'][OverallStatKeys[1]].values()) + list(
                game['stats'][OverallStatKeys[2]].values()) + list(game['stats'][OverallStatKeys[3]].values())

            stats.append(home)
            stats.append(avg)
            stats.append(winstreak)
            insert.append(team + stats)
            gamesplayed = gamesplayed + 1

        print(insert)
        print()
        connection = sqlite3.connect('../Database/sports_data.db')
        cur = connection.cursor()
        cur.execute(CreateDataBaseString)
        cur.executemany(InsertDataBaseString, insert)
        connection.commit()
    connection.close()



# seasonGameLogMLB(2019,datetime.date.today().year,"regular")
MLB_Website()
