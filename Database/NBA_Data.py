from ohmysportsfeedspy import MySportsFeeds
import sqlite3
import time
from Common import NBA_Dict
# from Common import NHL_Abbr_Conversion
# from NHL_NN import predict_Game
#import Bets
#import requests

# ---- Daily Games
import datetime

# ----------- Get Todays Game Schedule -------------------
def getNBADailyGames():
    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    Games = []
    date = str(datetime.date.today())
    date = date.replace("-", "")
    Output = Data_query.msf_get_data(league='nba', season='current', feed='daily_games', date=str(date),
                                     format='json', force='true')

    if not Output['games']:
        Output = Data_query.msf_get_data(league='nba', season='2018-2019-playoff', feed='daily_games', date=str(date),
                                         format='json', force='true')
        print(Output)
    for i in range(len(Output['games'])):
        Game = [Output['games'][i]['schedule']['homeTeam']['abbreviation'],
                Output['games'][i]['schedule']['awayTeam']['abbreviation']]
        Games.append(Game)

    return Games

def getNBAColums(yearLow, yearHigh, team):
    CreateDataBaseString= 'CREATE TABLE IF NOT EXISTS NBA_Team_Stats_Regular(Game_ID NUMERIC,Game_Date DATE, Away_Team_Abbv TEXT, Home_Team_Abbv TEXT, Team TEXT,'
    InsertDataBaseString = 'INSERT or ignore into NBA_Team_Stats_Regular(Game_ID,Game_Date, Away_Team_Abbv,Home_Team_Abbv, Team,'


    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    season = Data_query.msf_get_data(league='NBA', season=str(yearLow) + "-" + str(yearHigh)+ "-regular",
                                     feed='seasonal_team_gamelogs', team=NBA_Dict[team], format='json', force='true')

    print(season['gamelogs'])
    game = season['gamelogs'][0]#make sure to get a correct team abbv
    #field testing section --------------start delete

    # field testing section --------------end

    #----Put the colum headers in ---------------------------------
    StatKeys = list(game['stats'].keys()) #returned at bottom function ("batting", "Pitching" "Fielding" etc)
    numOfColums = 5 #init 5 see above collums

    for stat in game['stats']: #for batting, fielding, pitching
        columns = game['stats'][stat].keys() #get all the keys to batting, then fielding, then pithing etc concatinate
        for column in columns:
            CreateDataBaseString = CreateDataBaseString + str(column) + " REAL, "
            InsertDataBaseString = InsertDataBaseString + str(column) + ", "
            numOfColums += 1

    #----get value string  eg VALUES(?,?,?,?,?,? etc) -----------------
    valueString = 'VALUES('

    for i in range(numOfColums+3): #make the VALUES(?,?,?,?,?,? etc) section
        if i == numOfColums +2:
            valueString =  valueString + "?)"
        else:
            valueString = valueString + "?, "


    print(numOfColums)
    # #test--===========================================
    # create = create[0:-2] + ")"
    # statstring = statstring[0:-2] + ")"
    # sqlCreate = "CREATE TABLE IF NOT EXISTS NBA_Team_Stats_Regular_Test(" + create
    # sql = 'INSERT or ignore into NBA_Team_Stats_Regular_Test(' + statstring + valueString
    # print(sqlCreate)
    # print(sql)
    # print(numOfColums)
    # print(count)
    #
    #
    # s = []
    # stats = []
    # for game in season['gamelogs']:
    #     s = list(game['stats'][StatKeys[0]].values()) + list(game['stats'][StatKeys[1]].values()) + list(
    #         game['stats'][StatKeys[2]].values()) + list(game['stats'][StatKeys[3]].values()) + list(
    #         game['stats'][StatKeys[4]].values()) + list(game['stats'][StatKeys[5]].values()) + list(
    #         game['stats'][StatKeys[6]].values())
    #
    #
    #
    #     stats.append(s)
    #
    #
    # connection = sqlite3.connect('../Database/sports_data.db')
    # cur = connection.cursor()
    # cur.execute(sqlCreate)
    # cur.executemany(sql, stats)
    # connection.commit()
    # connection.close()

    # # -Clean Up and return -----------------
    CreateDataBaseString = CreateDataBaseString + "Home_Or_Away REAL, Five_Game_Avg REAL, Win_Streak REAL) "
    InsertDataBaseString = InsertDataBaseString + "Home_Or_Away, Five_Game_Avg, Win_Streak) " + valueString

    return CreateDataBaseString,InsertDataBaseString, StatKeys


# ------------ Get Season Game Logs -----------------------
# Puts data for the season in database and cleans all data
def seasonGameLogNBA(low_year, high_year, season_type):
    Data_query = MySportsFeeds('2.0', verbose=True)
    Data_query.authenticate('8d1666cd-a99f-40ac-92ad-d0fab4', 'MYSPORTSFEEDS')

    data = []

    for team in range(3, 31): #make sure this is the correct number for the teams in the leauge

        for year in range(low_year, high_year+1):
            CreateDataBaseString, InsertDataBaseString, OverallStatKeys = getNBAColums(year-1, year, team)
            print("getting " + NBA_Dict[team] + " year " + str(year))
            season = Data_query.msf_get_data(league='NBA', season=str(year-1)+ '-' + str(year) + '-' + season_type,
                                             feed='seasonal_team_gamelogs', team=NBA_Dict[team], format='json', force='true')

            data.append(season)
            time.sleep(10)

    if (NBA_Dict[team] == 15):
        time.sleep(330)
    team = []
    stats = []
    insert = []

    for season in data:
        gamesplayed = 0
        winstreak = 0
        insert.clear()
        for game in season['gamelogs']:
            avg = 0
            stats.clear()
            team.clear()

            if not game:
                continue

            if game['game']['homeTeamAbbreviation'] == game['team']['abbreviation']:
                home = 1
            else:
                home = 0
    #win streak and moving average
            if gamesplayed > 0:
                if gamesplayed >= 5:
                    for i in range(gamesplayed - 5, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / 5
                else:
                    for i in range(0, gamesplayed):
                        avg = season['gamelogs'][i]['stats']['standings']['wins'] + avg
                    avg = avg / gamesplayed

                if season['gamelogs'][gamesplayed - 1]['stats']['standings']['wins'] == 1:
                    winstreak = winstreak + 1
                else:
                    winstreak = 0

            team.append(game['game']['id'])
            team.append(game['game']['startTime'].split("T")[0])
            team.append(game['game']['awayTeamAbbreviation'])
            team.append(game['game']['homeTeamAbbreviation'])
            team.append(game['team']['abbreviation'])

            stats = list(game['stats'][OverallStatKeys[0]].values()) + list(game['stats'][OverallStatKeys[1]].values()) + list(
                game['stats'][OverallStatKeys[2]].values()) + list(game['stats'][OverallStatKeys[3]].values()) + list(
                game['stats'][OverallStatKeys[4]].values()) + list(game['stats'][OverallStatKeys[5]].values()) + list(
                game['stats'][OverallStatKeys[6]].values())

            stats.append(home)
            stats.append(avg)
            stats.append(winstreak)
            insert.append(team + stats)

            gamesplayed = gamesplayed + 1


        connection = sqlite3.connect('../Database/sports_data.db')
        cur = connection.cursor()
        cur.execute(CreateDataBaseString)
        #cur.executemany(InsertDataBaseString, insert)
        connection.commit()
    connection.close()

#getNBAColums(2016, 2017, 2)
year = datetime.date.today().year
seasonGameLogNBA(2016,year,"regular")